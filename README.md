# GLFW FFI

[GLFW][GLFW] 3.3 FFI bindings for Dart. Supports Windows and Linux desktops.

[GLFW]: https://www.glfw.org/