import 'dart:ffi';
import 'dart:convert';
import 'package:glfw/glfw.dart';
import 'package:glfw/src/glfw_init.dart';

void main() {
  initGlfw();

  glfwInit();
  print('GLFW: ${Utf8.fromUtf8(glfwGetVersionString().cast<Utf8>())}');
    
  var window = glfwCreateWindow(
    640, 480,
    Utf8.toUtf8('Dart FFI + GLFW + OpenGL'), 
    nullptr.cast(), nullptr.cast());

  while (glfwWindowShouldClose(window) != GLFW_TRUE)
  {
      glfwSwapBuffers(window);
      glfwPollEvents();
  }
} 

class Utf8 extends Struct<Utf8> {
  @Uint8()
  int char;

  static String fromUtf8(Pointer<Utf8> str) {
    List<int> units = [];
    int len = 0;
    while (true) {
      int char = str.elementAt(len++).load<Utf8>().char;
      if (char == 0) break;
      units.add(char);
    }
    return Utf8Decoder().convert(units);
  }

  static Pointer<Utf8> toUtf8(String s) {
    List<int> units = Utf8Encoder().convert(s);
    Pointer<Utf8> result =
        Pointer<Utf8>.allocate(count: units.length + 1).cast();
    for (int i = 0; i < units.length; i++) {
      result.elementAt(i).load<Utf8>().char = units[i];
    }
    result.elementAt(units.length).load<Utf8>().char = 0;
    return result;
  }
}